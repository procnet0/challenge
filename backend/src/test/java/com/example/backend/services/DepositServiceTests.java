package com.example.backend.services;

import com.example.backend.domains.Company;
import com.example.backend.domains.User;
import com.example.backend.domains.deposit.Deposit;
import com.example.backend.enums.DepositType;
import com.example.backend.repository.DepositRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.MockedStatic;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.AdditionalAnswers.returnsFirstArg;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class DepositServiceTests {

    @Mock
    private DepositRepository depositRepository;

    DepositService depositService;

    @BeforeEach
    void initService() {
        depositService = new DepositService(depositRepository);
    }

    @Test
    public void createDeposit_ShouldPass() {
        User user = new User();
        user.setId(1L);
        user.setName("john");
        Company company = new Company();
        company.setId(1L);
        company.setName("tesla");
        company.setBalance(10L);

        when(depositRepository.save((Mockito.any(Deposit.class)))).thenAnswer(returnsFirstArg());

        Deposit deposit = depositService.createDeposit(100L,DepositType.GIFT, user, company);
        assertThat(deposit).isNotNull();
        assertThat(deposit.getType()).isEqualTo(DepositType.GIFT);
        assertThat(deposit.getUser()).isNotNull();
        assertThat(deposit.getUser().getId()).isEqualTo(user.getId());
        assertThat(deposit.getCompany()).isNotNull();
        assertThat(deposit.getCompany().getId()).isEqualTo(company.getId());
        assertThat(deposit.getAmount()).isEqualTo(100L);

    }
    @Test
    public void createDeposit_ShouldNotPass_BadArgument() {
        User user = new User();
        user.setId(1L);
        user.setName("john");
        Company company = new Company();
        company.setId(1L);
        company.setName("tesla");
        company.setBalance(10L);

        Assertions.assertThrows(IllegalArgumentException.class, () -> {
            depositService.createDeposit(0L,DepositType.GIFT, user, company);
        }, "IllegalArgumentException was expected");
    }

    @Test
    public void createDeposit_ShouldNotPass_Null_User() {
        Company company = new Company();
        company.setId(1L);
        company.setName("tesla");
        company.setBalance(10L);

        Assertions.assertThrows(NullPointerException.class, () -> {
            depositService.createDeposit(100L,DepositType.GIFT, null, company);
        }, "NullPointerException was expected");

    }
    @Test
    public void createDeposit_ShouldNotPass_Null_Company() {
        User user = new User();
        user.setId(1L);
        user.setName("john");

        Assertions.assertThrows(NullPointerException.class, () -> {
            depositService.createDeposit(100L,DepositType.GIFT, user, null);
        }, "NullPointerException was expected");

    }
}
