package com.example.backend.services;

import com.example.backend.domains.Company;
import com.example.backend.domains.User;
import com.example.backend.domains.deposit.Deposit;
import com.example.backend.domains.deposit.GiftDeposit;
import com.example.backend.dto.DepositDto;
import com.example.backend.enums.DepositType;
import com.example.backend.exception.APIEntityNotFoundException;
import com.example.backend.exception.APIInsuficientBalanceException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class DispatchServiceTests {

    @Mock
    private UserService userService;
    @Mock
    private CompanyService companyService;
    @Mock
    private DepositService depositService;

    DispatchService dispatchService;

    @BeforeEach
    void initService() {
        dispatchService = new DispatchService(userService,companyService,depositService);
    }

    @Test
    public void dispatch_Should_Pass() {
        User user = new User();
        user.setId(1L);
        user.setName("john");
        Company company = new Company();
        company.setId(1L);
        company.setName("tesla");
        company.setBalance(1000L);
        DepositDto depositDto = new DepositDto(company.getId(), DepositType.GIFT,user.getId(), 100L);


        Deposit excpected = new GiftDeposit();
        excpected.setUser(user);
        excpected.setCompany((company));
        excpected.setAmount(100L);

        when(userService.findById(user.getId())).thenReturn(user);
        when(companyService.findById(company.getId())).thenReturn(company);
        when(depositService.createDeposit(depositDto.getAmount(),depositDto.getType(),user,company)).thenReturn(excpected);

        Deposit deposit = dispatchService.dispatchDeposit(depositDto);
        assertThat(deposit).isNotNull();
        assertThat(deposit.getUser()).isNotNull();
        assertThat(deposit.getCompany()).isNotNull();
    }

    @Test
    public void dispatch_Should_Throw_Insufficient_Balance() {
        User user = new User();
        user.setId(1L);
        user.setName("john");
        Company company = new Company();
        company.setId(1L);
        company.setName("tesla");
        company.setBalance(10L);
        DepositDto depositDto = new DepositDto(company.getId(), DepositType.GIFT,user.getId(), 100L);


        Deposit excpected = new GiftDeposit();
        excpected.setUser(user);
        excpected.setCompany((company));
        excpected.setAmount(100L);

        when(userService.findById(user.getId())).thenReturn(user);
        when(companyService.findById(company.getId())).thenReturn(company);

        Assertions.assertThrows(APIInsuficientBalanceException.class, () -> {
            dispatchService.dispatchDeposit(depositDto);
        }, "APIInsuficientBalanceException was expected");
    }
    @Test
    public void dispatch_Should_Throw_Null_User() {
        Company company = new Company();
        company.setId(1L);
        company.setName("tesla");
        company.setBalance(10L);
        DepositDto depositDto = new DepositDto(company.getId(), DepositType.GIFT,1L, 100L);

        when(userService.findById(1L)).thenReturn(null);
        when(companyService.findById(company.getId())).thenReturn(company);

        Assertions.assertThrows(APIEntityNotFoundException.class, () -> {
            dispatchService.dispatchDeposit(depositDto);
        }, "APIEntityNotFoundException was expected");
    }
    @Test
    public void dispatch_Should_Throw_Null_Company() {
        User user = new User();
        user.setId(1L);
        user.setName("john");
        DepositDto depositDto = new DepositDto(1L, DepositType.GIFT,user.getId(), 100L);


        when(userService.findById(user.getId())).thenReturn(user);
        when(companyService.findById(1L)).thenReturn(null);

        Assertions.assertThrows(APIEntityNotFoundException.class, () -> {
            dispatchService.dispatchDeposit(depositDto);
        }, "APIEntityNotFoundException was expected");

    }
    @Test
    public void dispatch_Should_NotPass_Null_Dto() {
        Assertions.assertThrows(NullPointerException.class, () -> {
            dispatchService.dispatchDeposit(null);
        }, "NullPointerException was expected");
    }
}
