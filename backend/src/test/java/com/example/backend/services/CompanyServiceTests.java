package com.example.backend.services;

import com.example.backend.domains.Company;
import com.example.backend.repository.CompanyRepository;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.NoSuchElementException;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class CompanyServiceTests {

    private final String companyName = "testCompany";
    private final Long companyBalance = 100L;
    private final Long companyId = 1L;

    @Mock
    private CompanyRepository companyRepository;

    CompanyService companyService;

    @BeforeEach
    void initService() {
        companyService = new CompanyService(companyRepository);
    }

    @AfterEach
    void afterEach() {
        companyService = null;
    }

    @Test
    public void findCompany_success() {
        Company company = new Company();
        company.setId(companyId);
        company.setBalance(companyBalance);
        company.setName(companyName);
        when(companyRepository.findById(companyId)).thenReturn(Optional.of(company));
        Company companyFetched = companyService.findById(companyId);
        assertThat(companyFetched).isNotNull();
        assertThat(companyFetched.getId()).isEqualTo(companyId);
        assertThat(companyFetched.getName()).isEqualTo(companyName);
        assertThat(companyFetched.getBalance()).isEqualTo(companyBalance);
    }

    @Test
    public void findCompany_failure_WithException() {
        when(companyRepository.findById(companyId)).thenReturn(Optional.empty());

        NoSuchElementException thrown = Assertions.assertThrows(NoSuchElementException.class, () -> {
            companyService.findById(companyId);
        }, "NoSuchElementException was expected");
        Assertions.assertEquals("No value present", thrown.getMessage());
    }
}
