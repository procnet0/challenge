package com.example.backend.services;

import com.example.backend.domains.User;
import com.example.backend.domains.deposit.Deposit;
import com.example.backend.domains.deposit.GiftDeposit;
import com.example.backend.exception.APIInsuficientBalanceException;
import com.example.backend.repository.UserRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.NoSuchElementException;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class UserServiceTests {
    @Mock
    private UserRepository userRepository;

    UserService userService;

    @BeforeEach
    void initService() {
        userService = new UserService(userRepository);
    }

    @Test
    public void findById_should_Pass() {

        User user = new User();
        user.setId(1L);
        user.setName("john");

        when(userRepository.findById(1L)).thenReturn(Optional.of(user));
        User result = userService.findById(1L);
        assertThat(result).isNotNull();
        assertThat(result.getName()).isEqualTo("john");
        assertThat(result.getBalance()).isNull();
        assertThat(result.getDeposits()).isNull();
    }
    @Test
    public void findById_should_Throw() {
        when(userRepository.findById(1L)).thenReturn(Optional.empty());
        Assertions.assertThrows(NoSuchElementException.class, () -> {
           userService.findById(1L);
        }, "NoSuchElementException was expected");
    }


    @Test
    public void findByIdWithRelationships_should_Pass() {
        User user = new User();
        user.setId(1L);
        user.setName("john");

        Deposit deposit = new GiftDeposit();
        deposit.setAmount(100L);
        deposit.setUser(user);

        user.setDeposits(Stream.of(deposit).collect(Collectors.toSet()));

        when(userRepository.findByIdWithEagerRelations(1L)).thenReturn(Optional.of(user));
        User result = userService.findByIdWithRelationships(1L);
        assertThat(result).isNotNull();
        assertThat(result.getName()).isEqualTo("john");
        assertThat(result.getBalance()).isEqualTo(100L);
        assertThat(result.getDeposits()).isNotNull();
        assertThat(result.getDeposits().size()).isEqualTo(1L);
    }
    @Test
    public void findByIdWithRelationships_should_Throw() {
        when(userRepository.findByIdWithEagerRelations(1L)).thenReturn(Optional.empty());
        Assertions.assertThrows(NoSuchElementException.class, () -> {
            userService.findByIdWithRelationships(1L);
        }, "NoSuchElementException was expected");
    }
}
