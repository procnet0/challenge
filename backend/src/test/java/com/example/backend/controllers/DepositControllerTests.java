package com.example.backend.controllers;

import com.example.backend.dto.DepositDto;
import com.example.backend.enums.DepositType;
import com.example.backend.exception.APIInsuficientBalanceException;
import com.example.backend.services.DispatchService;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(SpringExtension.class)
@WebMvcTest(DepositController.class)
public class DepositControllerTests {

    @MockBean
    DispatchService dispatchService;


    ObjectWriter ow = new ObjectMapper().writer().withDefaultPrettyPrinter();

    @Autowired
    private MockMvc mockMvc;

    @Test
    public void createDeposit_ShouldThrow_CompanyId() throws Exception {
        DepositDto depositDto = new DepositDto(null, DepositType.GIFT, 1L, 100L);

        when(dispatchService.dispatchDeposit(depositDto)).thenReturn(null);

        mockMvc.perform(MockMvcRequestBuilders.post("/deposit", depositDto)
                        .accept(MediaType.APPLICATION_JSON)
                        .content(ow.writeValueAsString(depositDto))
                        .contentType(MediaType.APPLICATION_JSON)
                )
                .andExpect(status().isBadRequest());


    }
    @Test
    public void createDeposit_ShouldThrow_amount() throws Exception {
        DepositDto depositDto = new DepositDto(1L, DepositType.GIFT, 1L, 0L);

        when(dispatchService.dispatchDeposit(depositDto)).thenReturn(null);

        mockMvc.perform(MockMvcRequestBuilders.post("/deposit", depositDto)
                        .accept(MediaType.APPLICATION_JSON)
                        .content(ow.writeValueAsString(depositDto))
                        .contentType(MediaType.APPLICATION_JSON)
                )
                .andExpect(status().isBadRequest());

        DepositDto depositDto2 = new DepositDto(1L, DepositType.GIFT, 1L, null);

        when(dispatchService.dispatchDeposit(depositDto2)).thenReturn(null);

        mockMvc.perform(MockMvcRequestBuilders.post("/deposit", depositDto2)
                        .accept(MediaType.APPLICATION_JSON)
                        .content(ow.writeValueAsString(depositDto2))
                        .contentType(MediaType.APPLICATION_JSON)
                )
                .andExpect(status().isBadRequest());
    }

    @Test
    public void createDeposit_ShouldThrow_UserId() throws Exception {
        DepositDto depositDto = new DepositDto(1L, DepositType.GIFT, null, 100L);

        when(dispatchService.dispatchDeposit(depositDto)).thenReturn(null);

        mockMvc.perform(MockMvcRequestBuilders.post("/deposit", depositDto)
                        .accept(MediaType.APPLICATION_JSON)
                        .content(ow.writeValueAsString(depositDto))
                        .contentType(MediaType.APPLICATION_JSON)
                )
                .andExpect(status().isBadRequest());


    }
}
