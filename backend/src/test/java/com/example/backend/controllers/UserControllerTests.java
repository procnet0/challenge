package com.example.backend.controllers;

import com.example.backend.domains.Company;
import com.example.backend.domains.User;
import com.example.backend.domains.deposit.Deposit;
import com.example.backend.enums.DepositType;
import com.example.backend.services.UserService;
import com.example.backend.utils.DepositUtils;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.time.LocalDate;
import java.util.Collections;
import java.util.HashSet;
import java.util.NoSuchElementException;
import java.util.Set;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(SpringExtension.class)
@WebMvcTest(UserController.class)
public class UserControllerTests {

    @MockBean
    UserService userService;

    @Autowired
    private MockMvc mockMvc;

    @Test
    public void getBalance_ShouldReturnBalance() throws Exception {
        // set up
        User user = new User();
        user.setId(1L);
        user.setName("User_With_Balance_0");
        user.setDeposits(Collections.emptySet());

        // inform
        when(userService.findByIdWithRelationships(1L)).thenReturn(user);

        // do
        mockMvc.perform(MockMvcRequestBuilders.get("/user/1/balance"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("balance").value(0));
    }
    @Test
    public void getBalance_ShouldThrow() throws Exception {
        // set up

        // inform
        when(userService.findByIdWithRelationships(1L)).thenThrow(NoSuchElementException.class);

        // do
        mockMvc.perform(MockMvcRequestBuilders.get("/user/1/balance"))
                .andExpect(status().is4xxClientError());
    }

    @Test
    public void getBalance_with_1_Gift_ShouldReturnBalance() throws Exception {
        // set up
        Company company = new Company();
        company.setId(1L);
        company.setBalance(100L);
        company.setName("Company_Test");

        Set<Deposit> deposits = new HashSet<>();

        Deposit deposit = new Deposit();
        deposit.setAmount(100L);
        deposit.setCompany(company);
        deposit.setType(DepositType.GIFT);
        deposit.setCreationDate(LocalDate.now());
        deposit.setExpirationDate(LocalDate.now().plusYears(1));

        deposits.add(deposit);

        User user = new User();
        user.setId(1L);
        user.setName("User_With_Balance_100");
        user.setDeposits(deposits);

        // inform
        when(userService.findByIdWithRelationships(1L)).thenReturn(user);

        // do
        mockMvc.perform(MockMvcRequestBuilders.get("/user/1/balance"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("balance").value(100L));
    }

    @Test
    public void getBalance_with_2_Gift_ShouldReturnBalance() throws Exception {
        // set up
        Company company = new Company();
        company.setId(1L);
        company.setBalance(100L);
        company.setName("Company_Test");

        Set<Deposit> deposits = new HashSet<>();

        Deposit deposit = new Deposit();
        deposit.setAmount(100L);
        deposit.setCompany(company);
        deposit.setType(DepositType.GIFT);
        deposit.setCreationDate(LocalDate.now());
        deposit.setExpirationDate(LocalDate.now().plusYears(1));

        Deposit deposit2 = new Deposit();
        deposit2.setAmount(100L);
        deposit2.setCompany(company);
        deposit2.setType(DepositType.GIFT);
        deposit2.setCreationDate(LocalDate.now());
        deposit2.setExpirationDate(LocalDate.now().plusYears(1));

        deposits.add(deposit);
        deposits.add(deposit2);

        User user = new User();
        user.setId(1L);
        user.setName("User_With_Balance_200");
        user.setDeposits(deposits);

        // inform
        when(userService.findByIdWithRelationships(1L)).thenReturn(user);

        // do
        mockMvc.perform(MockMvcRequestBuilders.get("/user/1/balance"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("balance").value(200L));
    }

    @Test
    public void getBalance_with_2_Mixed_ShouldReturnBalance() throws Exception {
        // set up
        Company company = new Company();
        company.setId(1L);
        company.setBalance(100L);
        company.setName("Company_Test");

        Set<Deposit> deposits = new HashSet<>();

        Deposit deposit = new Deposit();
        deposit.setAmount(100L);
        deposit.setCompany(company);
        deposit.setType(DepositType.MEAL);
        deposit.setCreationDate(LocalDate.now());
        deposit.setExpirationDate(DepositUtils.getMealExpirationDate(deposit.getCreationDate()));

        Deposit deposit2 = new Deposit();
        deposit2.setAmount(100L);
        deposit2.setCompany(company);
        deposit2.setType(DepositType.GIFT);
        deposit2.setCreationDate(LocalDate.now());
        deposit2.setExpirationDate(deposit2.getCreationDate().plusYears(1));

        deposits.add(deposit);
        deposits.add(deposit2);

        User user = new User();
        user.setId(1L);
        user.setName("User_With_Balance_200");
        user.setDeposits(deposits);

        // inform
        when(userService.findByIdWithRelationships(1L)).thenReturn(user);

        // do
        mockMvc.perform(MockMvcRequestBuilders.get("/user/1/balance"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("balance").value(200L));
    }
}
