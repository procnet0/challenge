package com.example.backend;

import com.example.backend.domains.Company;
import com.example.backend.domains.User;
import com.example.backend.domains.deposit.Deposit;
import com.example.backend.dto.DepositDto;
import com.example.backend.enums.DepositType;
import com.example.backend.repository.CompanyRepository;
import com.example.backend.repository.DepositRepository;
import com.example.backend.repository.UserRepository;
import com.example.backend.utils.TestDbService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;

import java.time.LocalDate;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;


/**
 *  Test that everything work as intended.
 */
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class IntegrationTests {

    @Autowired
    private TestRestTemplate restTemplate;
    @Autowired
    private TestDbService testDbService;
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private CompanyRepository companyRepository;
    @Autowired
    private DepositRepository depositRepository;

    private final String user1_name = "John";
    private final String user2_name = "Jane";
    private final String company1_name = "Tesla";
    private final String company2_name = "Apple";
    private final long company1_balance = 1000L;
    private final long company2_balance = 500L;
    private final long deposit_amount = 100L;

    private User user1;
    private User user2;
    private Company company1;
    private Company company2;

    @BeforeEach
    public void beforeEach() {
        // reset DB;
        testDbService.resetDatabase();
        // set Up DB;
        User user1 = new User(user1_name);
        User user2 = new User(user2_name);
        this.user1 = userRepository.save(user1);
        this.user2 = userRepository.save(user2);
        Company company1 = new Company(company1_name,company1_balance);
        Company company2 = new Company(company2_name,company2_balance);
        this.company1 = companyRepository.save(company1);
        this.company2 = companyRepository.save(company2);
    }


    @Test
    public void dispatchDeposit() {
        //arrange
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);

        DepositDto objEmp = new DepositDto(company1.getId(), DepositType.GIFT,user1.getId(), deposit_amount);
        HttpEntity<DepositDto> requestEntity = new HttpEntity<>(objEmp, headers);

        //act
        /**
         * give 100 to user 1 from company 1
         */
        ResponseEntity<Deposit> response = restTemplate.postForEntity("/deposit",requestEntity, Deposit.class);

        //assert
        assertThat(response.getStatusCodeValue()).isEqualTo(200);
        assertThat(response.getBody()).isNotNull();
        assertThat(response.getBody().getAmount()).isEqualTo(deposit_amount);
        assertThat(response.getBody().getCreationDate()).isEqualTo(LocalDate.now());
        assertThat(response.getBody().getExpirationDate()).isEqualTo(LocalDate.now().plusYears(1));
        assertThat(response.getBody().getCompany()).isNotNull();
        assertThat(response.getBody().getCompany().getId()).isEqualTo(company1.getId());
        assertThat(response.getBody().getUser()).isNotNull();
        assertThat(response.getBody().getUser().getId()).isEqualTo(user1.getId());
        assertThat(response.getBody().getCompany().getBalance()).isEqualTo(company1_balance - deposit_amount);
    }

    @Test
    public void getUserBalance(){
        //arrange
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);

        DepositDto objEmp = new DepositDto(company1.getId(), DepositType.GIFT,user1.getId(), deposit_amount);
        HttpEntity<DepositDto> requestEntity = new HttpEntity<>(objEmp, headers);

        //act
        /**
         * give 100 to user 1 from company 1
         */
        restTemplate.postForEntity("/deposit",requestEntity, Deposit.class);
        objEmp = new DepositDto(company2.getId(), DepositType.MEAL,user1.getId(), deposit_amount);
        requestEntity = new HttpEntity<>(objEmp, headers);
        restTemplate.postForEntity("/deposit",requestEntity, Deposit.class);
        /**
         * retrieve User balance
         */
        ResponseEntity<User> response = restTemplate.getForEntity("/user/1/balance", User.class);

        //assert
        assertThat(response.getStatusCodeValue()).isEqualTo(200);
        assertThat(response.getBody()).isNotNull();
        assertThat(response.getBody().getBalance()).isEqualTo(deposit_amount * 2);

        Company company = companyRepository.findById(company1.getId()).orElseThrow();
        assertThat(company.getBalance()).isEqualTo(company1_balance - deposit_amount);
        company = companyRepository.findById(company2.getId()).orElseThrow();
        assertThat(company.getBalance()).isEqualTo(company2_balance - deposit_amount);
    }

    @Test
    public void getUserBalanceWith_OldDeposit(){
        //arrange
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);

        DepositDto objEmp = new DepositDto(company1.getId(), DepositType.GIFT,user1.getId(), deposit_amount);
        HttpEntity<DepositDto> requestEntity = new HttpEntity<>(objEmp, headers);

        //act
        /**
         * give 100 to user 1 from company 1
         */
        restTemplate.postForEntity("/deposit",requestEntity, Deposit.class);
        objEmp = new DepositDto(company2.getId(), DepositType.MEAL,user1.getId(), deposit_amount);
        requestEntity = new HttpEntity<>(objEmp, headers);
        restTemplate.postForEntity("/deposit",requestEntity, Deposit.class);

        List<Deposit> deposits = depositRepository.findAll();
        assertThat(deposits.size()).isEqualTo(2);
        Deposit updatedDeposit = deposits.get(0);
        updatedDeposit.setCreationDate(updatedDeposit.getCreationDate().minusYears(2));
        updatedDeposit.setExpirationDate(updatedDeposit.getExpirationDate().minusYears(2));
        depositRepository.save(updatedDeposit);

        /**
         * retrieve User balance
         */
        ResponseEntity<User> response = restTemplate.getForEntity("/user/1/balance", User.class);

        //assert
        assertThat(response.getStatusCodeValue()).isEqualTo(200);
        assertThat(response.getBody()).isNotNull();
        assertThat(response.getBody().getBalance()).isEqualTo(deposit_amount * 1);

        Company company = companyRepository.findById(company1.getId()).orElseThrow();
        assertThat(company.getBalance()).isEqualTo(company1_balance - deposit_amount);
        company = companyRepository.findById(company2.getId()).orElseThrow();
        assertThat(company.getBalance()).isEqualTo(company2_balance - deposit_amount);
    }

    @Test
    public void getUserBalanceWith_NoDeposit(){
        //arrange

        //act
        /**
         * retrieve User balance
         */
        ResponseEntity<User> response = restTemplate.getForEntity("/user/1/balance", User.class);

        //assert
        assertThat(response.getStatusCodeValue()).isEqualTo(200);
        assertThat(response.getBody()).isNotNull();
        assertThat(response.getBody().getBalance()).isEqualTo(0);
    }
}
