package com.example.backend.domains;

import com.example.backend.exception.messages.ErrorMessages;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class CompanyClassTest {

    private final String validName = "Tesla";
    private final String invalid_Empty_Name = "";
    private final String invalid_TooLong_Name = "VeryLongCompanyNameWithMoreThanAuthorizedSizeForColumnInDatabase";

    private final Long validAmount = 100L;
    private final Long invalidNegativeAmount = -100L;

    @Test
    public void createCompany_Should_Pass() {

        Company company = new Company(validName, validAmount);
        assertThat(company).isNotNull();
        assertThat(company.getName()).isEqualTo(validName);
        assertThat(company.getBalance()).isEqualTo(validAmount);
    }
    @Test
    public void createCompany_ShouldNot_Pass_Invalid_Balance() {
        IllegalArgumentException thrown = Assertions.assertThrows(IllegalArgumentException.class, () -> {
            new Company(validName, invalidNegativeAmount);
        }, "IllegalArgumentException was expected");
        Assertions.assertEquals("Balance :"+ ErrorMessages.MustBePositive, thrown.getMessage());
    }
    @Test
    public void createCompany_ShouldNot_Pass_Null_Balance() {
        NullPointerException thrown = Assertions.assertThrows(NullPointerException.class, () -> {
            new Company(validName, null);
        }, "IllegalArgumentException was expected");
        Assertions.assertEquals("Balance :"+ ErrorMessages.MustNotBeNull, thrown.getMessage());
    }
    @Test
    public void createCompany_ShouldNot_Pass_Empty_Name() {
        IllegalArgumentException thrown = Assertions.assertThrows(IllegalArgumentException.class, () -> {
            new Company(invalid_Empty_Name, validAmount);
        }, "IllegalArgumentException was expected");
        Assertions.assertEquals("Name :"+ ErrorMessages.MustNotBeEmpty, thrown.getMessage());
    }

    @Test
    public void createCompany_ShouldNot_Pass_Null_Name() {
        NullPointerException thrown = Assertions.assertThrows(NullPointerException.class, () -> {
            new Company(null, validAmount);
        }, "NullPointerException was expected");
        Assertions.assertEquals("Name :"+ ErrorMessages.MustNotBeNull, thrown.getMessage());
    }

    @Test
    public void createCompany_ShouldNot_Pass_TooLong_Name() {
        IllegalArgumentException thrown = Assertions.assertThrows(IllegalArgumentException.class, () -> {
            new Company(invalid_TooLong_Name, validAmount);
        }, "IllegalArgumentException was expected");
        Assertions.assertEquals("Name :"+ ErrorMessages.IsTooLong, thrown.getMessage());
    }

    @Test
    public void test_toString() {

        Company company = new Company(validName, validAmount);
        company.setId(1L);
        assertThat(company.toString())
                .isEqualTo("Company{" +
                        "id=" + 1L +
                        ", name='" + validName + '\'' +
                        ", balance=" + validAmount +
                        '}');
    }
}
