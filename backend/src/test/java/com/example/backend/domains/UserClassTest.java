package com.example.backend.domains;

import com.example.backend.exception.messages.ErrorMessages;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;

public class UserClassTest {
    private final String validName = "Tesla";
    private final String invalid_Empty_Name = "";
    private final String invalid_TooLong_Name = "VeryLongUserNameWithMoreThanAuthorizedSizeForColumnInDatabase";

    @Test
    public void createCompany_ShouldNot_Pass_Empty_Name() {
        IllegalArgumentException thrown = Assertions.assertThrows(IllegalArgumentException.class, () -> {
            new User(invalid_Empty_Name);
        }, "IllegalArgumentException was expected");
        Assertions.assertEquals("Name :"+ ErrorMessages.MustNotBeEmpty, thrown.getMessage());
    }
    @Test
    public void createCompany_ShouldNot_Pass_TooLong_Name() {
        IllegalArgumentException thrown = Assertions.assertThrows(IllegalArgumentException.class, () -> {
            new User(invalid_TooLong_Name);
        }, "IllegalArgumentException was expected");
        Assertions.assertEquals("Name :"+ ErrorMessages.IsTooLong, thrown.getMessage());
    }
    @Test
    public void createCompany_ShouldNot_Pass_Null_Name() {
        NullPointerException thrown = Assertions.assertThrows(NullPointerException.class, () -> {
            new User(null);
        }, "NullPointerException was expected");
        Assertions.assertEquals("Name :"+ ErrorMessages.MustNotBeNull, thrown.getMessage());
    }
    @Test
    public void test_toString() {

        User user = new User( "john");
        user.setId(1L);
        assertThat(user.toString())
                .isEqualTo("User{" +
                        "id=" + 1L +
                        ", name='" + user.getName() + '\'' +
                        ", balance=" + user.getBalance() +
                        '}');
    }
}
