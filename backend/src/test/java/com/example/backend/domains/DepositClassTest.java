package com.example.backend.domains;

import com.example.backend.domains.deposit.Deposit;
import com.example.backend.domains.deposit.GiftDeposit;
import com.example.backend.domains.deposit.MealDeposit;
import com.example.backend.enums.DepositType;
import com.example.backend.exception.messages.ErrorMessages;
import com.example.backend.utils.DepositUtils;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class DepositClassTest {

    private final Long validAmount = 100L;
    private final Long unvalidNegativeAmount = -100L;
    private final Long unvalidNullAmount = null;


    @Test
    public void create_Gift() {
        Deposit gift = new GiftDeposit(validAmount);
        assertThat(gift).isNotNull();
        assertThat(gift.getCreationDate()).isNotNull();
        assertThat(gift.getType()).isEqualTo(DepositType.GIFT);
        assertThat(gift.getExpirationDate()).isEqualTo(gift.getCreationDate().plusYears(1));
    }
    @Test
    public void create_Meal() {
        Deposit meal = new MealDeposit(validAmount);
        assertThat(meal).isNotNull();
        assertThat(meal.getCreationDate()).isNotNull();
        assertThat(meal.getType()).isEqualTo(DepositType.MEAL);
        assertThat(meal.getExpirationDate()).isEqualTo(DepositUtils.getMealExpirationDate(meal.getCreationDate()));
    }
    @Test
    public void create_Gift_And_Set_Data() {
        User user = new User();
        user.setId(1L);
        user.setName("Test User");

        Company company = new Company();
        company.setId(1L);
        company.setName("Test Company");

        Deposit gift = new GiftDeposit(validAmount);
        gift.setUser(user);
        gift.setCompany(company);

        assertThat(gift).isNotNull();
        assertThat(gift.getCreationDate()).isNotNull();
        assertThat(gift.getType()).isEqualTo(DepositType.GIFT);
        assertThat(gift.getExpirationDate()).isEqualTo(gift.getCreationDate().plusYears(1));
        assertThat(gift.getAmount()).isEqualTo(validAmount);
        assertThat(gift.getUser()).isNotNull();
        assertThat(gift.getUser().getId()).isEqualTo(1L);
        assertThat(gift.getCompany()).isNotNull();
        assertThat(gift.getCompany().getId()).isEqualTo(1L);
    }

    @Test
    public void create_Meal_And_Set_Data() {
        User user = new User();
        user.setId(1L);
        user.setName("Test User");

        Company company = new Company();
        company.setId(1L);
        company.setName("Test Company");

        Deposit meal = new MealDeposit(validAmount);
        meal.setUser(user);
        meal.setCompany(company);

        assertThat(meal).isNotNull();
        assertThat(meal.getCreationDate()).isNotNull();
        assertThat(meal.getType()).isEqualTo(DepositType.MEAL);
        assertThat(meal.getExpirationDate()).isEqualTo(DepositUtils.getMealExpirationDate(meal.getCreationDate()));
        assertThat(meal.getAmount()).isEqualTo(validAmount);
        assertThat(meal.getUser()).isNotNull();
        assertThat(meal.getUser().getId()).isEqualTo(1L);
        assertThat(meal.getCompany()).isNotNull();
        assertThat(meal.getCompany().getId()).isEqualTo(1L);
    }

    @Test
    public void create_Meal_With_Negative_Amount_Should_Throw() {
        IllegalArgumentException thrown = Assertions.assertThrows(IllegalArgumentException.class, () -> {
            Deposit meal = new MealDeposit(unvalidNegativeAmount);
        }, "IllegalArgumentException was expected");
        Assertions.assertEquals("Amount :"+ ErrorMessages.MustBeStrictPositive, thrown.getMessage());
    }
    @Test
    public void create_Deposit_With_Null_Amount_Should_Throw() {
        NullPointerException thrown = Assertions.assertThrows(NullPointerException.class, () -> {
            new MealDeposit(unvalidNullAmount);
        }, "NullPointerException was expected");
        Assertions.assertEquals("Amount :"+ ErrorMessages.MustNotBeNull, thrown.getMessage());

    }

    @Test
    public void create_Deposit_With_Zero_Amount_Should_Throw() {
        IllegalArgumentException thrown = Assertions.assertThrows(IllegalArgumentException.class, () -> {
            Deposit meal = new MealDeposit(0L);
        }, "IllegalArgumentException was expected");
        Assertions.assertEquals("Amount :"+ ErrorMessages.MustBeStrictPositive, thrown.getMessage());
    }
    @Test
    public void test_toString() {

        Deposit gift = new GiftDeposit(validAmount);
        gift.setId(1L);
        assertThat(gift.toString())
                .isEqualTo("Deposit{" +
                        "id=" + 1L +
                        ", type=" + gift.getType() +
                        ", creationDate=" + gift.getCreationDate() +
                        ", expirationDate=" + gift.getExpirationDate() +
                        ", amount=" + validAmount +
                        ", user=" + gift.getUser() +
                        ", company=" + gift.getCompany() +
                        '}');
    }

}
