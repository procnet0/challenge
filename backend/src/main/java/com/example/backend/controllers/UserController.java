package com.example.backend.controllers;

import com.example.backend.domains.User;
import com.example.backend.exception.APIEntityNotFoundException;
import com.example.backend.services.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.NoSuchElementException;

@RestController
@RequestMapping("/user")
public class UserController {

    Logger logger = LoggerFactory.getLogger(UserController.class);

    private final UserService userService;

    @Autowired
    UserController(UserService userService) {
        this.userService = userService;
    }

    @GetMapping("/{id}/balance")
    public ResponseEntity<User> getUserWithBalance(@PathVariable Long id) {
        try {
            User user = userService.findByIdWithRelationships(id);
            return ResponseEntity.ok(user);
        } catch (NoSuchElementException e) {
            throw new APIEntityNotFoundException("User with id="+id);
        }
    }
}
