package com.example.backend.controllers;

import com.example.backend.domains.deposit.Deposit;
import com.example.backend.dto.DepositDto;
import com.example.backend.exception.APIBadRequestException;
import com.example.backend.services.DispatchService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/deposit")
public class DepositController {

    Logger logger = LoggerFactory.getLogger(DepositController.class);

    private final DispatchService dispatchService;

    @Autowired
    public DepositController(DispatchService dispatchService) {

        this.dispatchService = dispatchService;
    }

    @PostMapping
    public ResponseEntity<Deposit> createDeposit(@RequestBody DepositDto depositData) {

        if(depositData.getUserId() == null){
            throw new APIBadRequestException("UserId can't be null");
        }
        if(depositData.getCompanyId() == null){
            throw new APIBadRequestException("CompanyId can't be null");
        }
        if(depositData.getAmount() == null || depositData.getAmount() <= 0){
            throw new APIBadRequestException("Amount can't be null, equal or inferior to 0");
        }
        Deposit deposit = dispatchService.dispatchDeposit(depositData);
        return ResponseEntity.ok(deposit);
    }
}
