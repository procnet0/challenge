package com.example.backend.dto;

import com.example.backend.enums.DepositType;

public class DepositDto {
    private final Long companyId;
    private final DepositType type;
    private final Long userId;
    private final Long amount;

    public DepositDto(Long companyId, DepositType type, Long userId, Long amount) {
        this.companyId = companyId;
        this.type = type;
        this.userId = userId;
        this.amount = amount;
    }

    public Long getCompanyId() {
        return companyId;
    }

    public Long getUserId() {
        return userId;
    }

    public Long getAmount() {
        return amount;
    }

    public DepositType getType() {
        return type;
    }

    @Override
    public String toString() {
        return "DepositDto{" +
                "companyId=" + companyId +
                ", type=" + type +
                ", userId=" + userId +
                ", amount=" + amount +
                '}';
    }
}
