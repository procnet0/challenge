package com.example.backend.utils;

import java.time.LocalDate;
import java.time.temporal.TemporalAdjusters;

public class DepositUtils {

    /**
     *
     * @param localDate
     * @return : next year last day of February starting from localdate
     */
    public static LocalDate getMealExpirationDate(LocalDate localDate) {
        return localDate.plusYears(1).withMonth(2).with(TemporalAdjusters.lastDayOfMonth());
    }
}
