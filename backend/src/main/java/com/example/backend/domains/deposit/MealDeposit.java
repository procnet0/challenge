package com.example.backend.domains.deposit;

import com.example.backend.enums.DepositType;
import com.example.backend.utils.DepositUtils;

import javax.persistence.Entity;
import java.time.LocalDate;

@Entity
public class MealDeposit extends Deposit{
    public MealDeposit() {
        init();
    }
    public MealDeposit(Long amount) throws IllegalArgumentException,NullPointerException {

        init();
        setAmount(amount);
    }

    public void init() {
        setType(DepositType.MEAL);
        setCreationDate(LocalDate.now());
        setExpirationDate(DepositUtils.getMealExpirationDate(getCreationDate()));

    }
}
