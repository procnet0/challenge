package com.example.backend.domains.deposit;

import com.example.backend.domains.Company;
import com.example.backend.domains.User;
import com.example.backend.enums.DepositType;
import com.example.backend.exception.messages.ErrorMessages;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
public class Deposit {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Enumerated(EnumType.STRING)
    private DepositType type;

    @Column( columnDefinition = "DATE")
    private LocalDate creationDate;

    @Column(columnDefinition = "DATE")
    private LocalDate expirationDate;

    @Column(nullable = false)
    private Long amount;

    @ManyToOne
    @JsonIgnoreProperties("deposits")
    private User user;

    @ManyToOne
    @JsonIgnoreProperties("deposit")
    private Company company;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public DepositType getType() {
        return type;
    }

    public void setType(DepositType type) {
        this.type = type;
    }

    public LocalDate getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(LocalDate creationDate) {
        this.creationDate = creationDate;
    }

    public LocalDate getExpirationDate() {
        return expirationDate;
    }

    public void setExpirationDate(LocalDate expirationDate) {
        this.expirationDate = expirationDate;
    }

    public Long getAmount() {
        return amount;
    }

    public void setAmount(Long amount) throws IllegalArgumentException,NullPointerException {
        if(amount == null) {
            throw new NullPointerException("Amount :" + ErrorMessages.MustNotBeNull);
        }
        if( amount <= 0) {
            throw new IllegalArgumentException("Amount :" + ErrorMessages.MustBeStrictPositive);
        }
        this.amount = amount;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Company getCompany() {
        return company;
    }

    public void setCompany(Company company) {
        this.company = company;
    }

    @Override
    public String toString() {
        return "Deposit{" +
                "id=" + id +
                ", type=" + type +
                ", creationDate=" + creationDate +
                ", expirationDate=" + expirationDate +
                ", amount=" + amount +
                ", user=" + user +
                ", company=" + company +
                '}';
    }
}
