package com.example.backend.domains;

import com.example.backend.domains.deposit.Deposit;
import com.example.backend.exception.messages.ErrorMessages;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;
import java.util.Set;

@Entity
public class Company {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;


    @Column(length=50, nullable=false, unique=true)
    private String name;

    @OneToMany(mappedBy="company", fetch=FetchType.LAZY)
    @JsonIgnoreProperties(value={"user","company"})
    private Set<Deposit> deposits;

    private Long balance;

    public Company() {
    }

    public Company(String name, Long balance) throws NullPointerException, IllegalArgumentException {
        this.setName(name);
        this.setBalance(balance);
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) throws NullPointerException, IllegalArgumentException{
        if(name == null) {
            throw new NullPointerException("Name :" + ErrorMessages.MustNotBeNull);
        }
        if(name.length() == 0) {
            throw new IllegalArgumentException("Name :" + ErrorMessages.MustNotBeEmpty);
        }
        if(name.length() > 50) {
            throw new IllegalArgumentException("Name :" + ErrorMessages.IsTooLong);
        }
        this.name = name;
    }

    public Set<Deposit> getDeposit() {
        return deposits;
    }

    public void setDeposit(Set<Deposit> deposit) {
        this.deposits = deposit;
    }

    public Long getBalance() {
        return balance;
    }

    public void setBalance(Long balance) throws NullPointerException, IllegalArgumentException {
        if(balance == null) {
            throw new NullPointerException("Balance :" + ErrorMessages.MustNotBeNull);
        }
        if(balance < 0) {
            throw new IllegalArgumentException("Balance :" + ErrorMessages.MustBePositive);
        }
        this.balance = balance;
    }

    @Override
    public String toString() {
        return "Company{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", balance=" + balance +
                '}';
    }


}
