package com.example.backend.domains.deposit;

import com.example.backend.enums.DepositType;

import javax.persistence.Entity;
import java.time.LocalDate;

@Entity
public class GiftDeposit extends Deposit{
    public GiftDeposit() {
        init();
    }
    public GiftDeposit(Long amount) throws IllegalArgumentException,NullPointerException{
        init();
        setAmount(amount);
    }

    public void init() {
        setType(DepositType.GIFT);
        setCreationDate(LocalDate.now());
        setExpirationDate(getCreationDate().plusYears(1));
    }
}
