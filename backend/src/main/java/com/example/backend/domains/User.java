package com.example.backend.domains;

import com.example.backend.domains.deposit.Deposit;
import com.example.backend.exception.messages.ErrorMessages;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Where;
import javax.persistence.*;
import java.util.Set;
import java.util.stream.Stream;

@Entity
public class User  {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(length=50, nullable=false)
    private String name;

    @OneToMany(mappedBy="user", fetch=FetchType.EAGER)
    @JsonIgnoreProperties(value={"user","company"},allowSetters = true)
    @Where(clause="expiration_date > CAST( now() AS Date ) ")
    private Set<Deposit> deposits;

    @Transient
    private Long balance;

    public User() {
    }

    public User(String name) {
        this.setName(name);
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        if(name == null) {
            throw new NullPointerException("Name :" + ErrorMessages.MustNotBeNull);
        }
        if(name.length() == 0) {
            throw new IllegalArgumentException("Name :" + ErrorMessages.MustNotBeEmpty);
        }
        if(name.length() > 50) {
            throw new IllegalArgumentException("Name :" + ErrorMessages.IsTooLong);
        }
        this.name = name;
    }

    public Set<Deposit> getDeposits() {
        return deposits;
    }

    public void setDeposits(Set<Deposit> deposits) {
        this.deposits = deposits;
    }

    public Long getBalance() {
        this.balance = this.getDeposits() != null ? this.getDeposits().stream().flatMap(Stream::ofNullable).mapToLong(Deposit::getAmount).sum() : null;
        return balance;
    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", balance=" + balance +
                '}';
    }

}
