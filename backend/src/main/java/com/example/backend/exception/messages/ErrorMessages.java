package com.example.backend.exception.messages;

public class ErrorMessages {

    public static String MustNotBeNull = "must not be null";
    public static String MustBePositive = "must be positive";
    public static String MustBeStrictPositive = "must be strictly positive";
    public static String MustNotBeEmpty = "must not be empty";
    public static String IsGreaterThanMaxValue = "exceeding maximum value";

    public static String IsTooLong = "is to long";
    public static String IsTooShort = "is to short";
}
