package com.example.backend.exception;

import com.sun.istack.NotNull;

public class APIServerException extends APIException {

    public APIServerException(@NotNull String info) {
        super(50, "An unhandled error occurred, info: " + info);
    }
}
