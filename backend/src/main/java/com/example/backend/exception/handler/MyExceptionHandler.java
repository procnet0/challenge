package com.example.backend.exception.handler;

import com.example.backend.exception.*;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@RestControllerAdvice
public class MyExceptionHandler extends ResponseEntityExceptionHandler {

    @ExceptionHandler(value = { APIBadRequestException.class })
    protected ResponseEntity<Object> handleGenericExceptions(APIBadRequestException ex, WebRequest request) {
        return ResponseEntity.status( HttpStatus.BAD_REQUEST ).body(ex.message());
    }
    @ExceptionHandler(value = { APIInsuficientBalanceException.class })
    protected ResponseEntity<Object> handleGenericExceptions(APIInsuficientBalanceException ex, WebRequest request) {
        return ResponseEntity.status( HttpStatus.BAD_REQUEST ).body(ex.message());
    }
    @ExceptionHandler(value = { APIEntityNotFoundException.class })
    protected ResponseEntity<Object> handleGenericExceptions(APIEntityNotFoundException ex, WebRequest request) {
        return ResponseEntity.status( HttpStatus.NOT_FOUND ).body(ex.message());
    }
    @ExceptionHandler(value = { APIServerException.class })
    protected ResponseEntity<Object> handleGenericExceptions(APIServerException ex, WebRequest request) {
        return ResponseEntity.status( HttpStatus.INTERNAL_SERVER_ERROR ).body(ex.message());
    }
}
