package com.example.backend.exception;

import com.sun.istack.NotNull;

public class APIBadRequestException  extends APIException{
    public APIBadRequestException(@NotNull String info) {
        super(1, "BadRequest: "+ info);
    }
}
