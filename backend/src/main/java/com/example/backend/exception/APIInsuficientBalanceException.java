package com.example.backend.exception;

import com.sun.istack.NotNull;

public class APIInsuficientBalanceException extends  APIException {

    public APIInsuficientBalanceException(@NotNull String entityName) {
        super(20, "Entity "+ entityName +" do not have sufficient balance.");
    }
}
