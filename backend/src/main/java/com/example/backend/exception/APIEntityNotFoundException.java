package com.example.backend.exception;

import com.sun.istack.NotNull;

public class APIEntityNotFoundException extends APIException {

    public APIEntityNotFoundException(@NotNull String entityName) {
        super(10, "Entity "+ entityName +" could not be found.");
    }
}
