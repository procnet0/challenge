package com.example.backend.services;

import com.example.backend.domains.Company;
import com.example.backend.domains.User;
import com.example.backend.domains.deposit.Deposit;
import com.example.backend.dto.DepositDto;
import com.example.backend.enums.NumericalOperation;
import com.example.backend.exception.APIEntityNotFoundException;
import com.example.backend.exception.APIInsuficientBalanceException;
import com.example.backend.exception.APIServerException;
import com.sun.istack.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.NoSuchElementException;

@Service
@Transactional
public class DispatchService {

    private final UserService userService;
    private final CompanyService companyService;
    private final DepositService depositService;

    @Autowired
    public DispatchService(UserService userService, CompanyService companyService, DepositService depositService) {
        this.userService = userService;
        this.companyService = companyService;
        this.depositService = depositService;
    }

    /**
     *
     * @param depositData the Data of the deposit to be created
     * @return the newly Created Deposit
     */
    public Deposit dispatchDeposit(@NotNull DepositDto depositData) {
        User userTo = userService.findById(depositData.getUserId());
        Company companyFrom = companyService.findById(depositData.getCompanyId());

        if(userTo == null ){
            throw new APIEntityNotFoundException("User with id="+ depositData.getUserId());
        }
        if(companyFrom == null) {
            throw new APIEntityNotFoundException("Company with id="+ depositData.getCompanyId());
        }
        if(companyFrom.getBalance() < depositData.getAmount()) {
            throw new APIInsuficientBalanceException("Company with id="+ depositData.getCompanyId());
        }

        try {
            Deposit deposit = depositService.createDeposit(depositData.getAmount(),depositData.getType(), userTo, companyFrom);
            companyService.updateBalance(companyFrom.getId(), deposit.getAmount(), NumericalOperation.SUBTRACT);
            return deposit;
        } catch (NoSuchElementException | NullPointerException e) {
            throw new APIEntityNotFoundException(e.getMessage());
        } catch (IllegalArgumentException | APIInsuficientBalanceException e) {
            e.printStackTrace();
            throw new APIServerException(e.getMessage());
        }

    }
}
