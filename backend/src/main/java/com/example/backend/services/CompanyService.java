package com.example.backend.services;

import com.example.backend.domains.Company;
import com.example.backend.enums.NumericalOperation;
import com.example.backend.exception.APIInsuficientBalanceException;
import com.example.backend.repository.CompanyRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.NoSuchElementException;

@Service
@Transactional
public class CompanyService {

    private final CompanyRepository companyRepository;

    @Autowired
    CompanyService(CompanyRepository companyRepository)
    {
        this.companyRepository = companyRepository;
    }

    public Company findById(long companyId)  throws NoSuchElementException {
        return companyRepository.findById(companyId).orElseThrow();
    }

    public Company save(Company companyFrom) {
       return companyRepository.save(companyFrom);
    }

    public Company updateBalance(Long id, Long amount, NumericalOperation operation) throws NoSuchElementException {
        Company company = companyRepository.findById(id).orElseThrow();


        switch(operation) {
            case ADD:
                company.setBalance(company.getBalance() + amount );
                break;
            case SUBTRACT:
                if(company.getBalance() - amount < 0) {
                    throw new APIInsuficientBalanceException("Company");
                }
                company.setBalance(company.getBalance() - amount );
                break;
        }
        company = companyRepository.save(company);
        return company;
    }
}
