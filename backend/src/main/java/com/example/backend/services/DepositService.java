package com.example.backend.services;

import com.example.backend.domains.Company;
import com.example.backend.domains.User;
import com.example.backend.domains.deposit.Deposit;
import com.example.backend.domains.deposit.GiftDeposit;
import com.example.backend.domains.deposit.MealDeposit;
import com.example.backend.enums.DepositType;
import com.example.backend.repository.DepositRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.annotation.Validated;


@Service
@Transactional
public class DepositService {

    private final DepositRepository depositRepository;

    @Autowired
    DepositService(DepositRepository depositRepository) {
        this.depositRepository = depositRepository;
    }

    @Validated
    public Deposit createDeposit(Long amount, DepositType type, User user,  Company company) throws IllegalArgumentException,NullPointerException {
        if(user == null) {
            throw new NullPointerException("User cannot be null");
        }
        if(company == null) {
            throw new NullPointerException("Company cannot be null");
        }
        Deposit deposit;
        switch(type){
            case GIFT:
                deposit = new GiftDeposit(amount);
                break;
            case MEAL:
                deposit = new MealDeposit(amount);
                break;
            default:
                throw new IllegalArgumentException();
        }
        deposit.setUser(user);
        deposit.setCompany(company);
        return depositRepository.save(deposit);
    }

}
