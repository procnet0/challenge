package com.example.backend.services;

import com.example.backend.domains.User;
import com.example.backend.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.NoSuchElementException;

@Service
@Transactional
public class UserService {

    private final UserRepository userRepository;

    @Autowired
    public UserService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    /**
     *
     * @return User without Lazy Relationship loaded.
     */
    public User findById(long userId) {
        return userRepository.findById(userId).orElseThrow();
    }

    /**
     *
     * @return User with Deposit lazily loaded.
     */
    public User findByIdWithRelationships(long userId) throws NoSuchElementException {
        User user = userRepository.findByIdWithEagerRelations(userId).orElseThrow();
        return user;
    }
}
